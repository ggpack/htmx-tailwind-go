package main

import "log"
import "net/http"

func main() {
	log.Printf("Starting the server")

	app := newApp()

	server := &http.Server{
		Addr:    "0.0.0.0:8080",
		Handler: app,
	}

	log.Printf("HTTP server listening on %v", server.Addr)
	log.Fatal(server.ListenAndServe())
}