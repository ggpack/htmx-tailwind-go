/** @type {import('tailwindcss').Config} */

// https://tailwindcss.com/docs/customizing-colors#using-css-variables

const content = process.env.NODE_ENV === "production" ? ["**/*.html"] : []

module.exports = {
	darkMode: "class",
	content,
	theme: {
		colors: {
			color_bkg: 'rgb(var(--color-bkg) / <alpha-value>)',
			color_content: 'rgb(var(--color-content) / <alpha-value>)',
		}
	},
	plugins: [],
}

