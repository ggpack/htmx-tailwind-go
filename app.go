package main

import (
	"embed"
	"html/template"
	"log"
	"net/http"
)

//go:embed content
var content embed.FS

//go:embed templates
var allFS embed.FS

var allTemplates = template.Must(template.New("").ParseFS(allFS, "templates/*"))

func logReq(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("New request on: '%s %s'", r.Method, r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func newApp() http.Handler {
	log.Printf("Init the backend")
	log.Printf("les templates: %s", allTemplates.DefinedTemplates())

	mux := http.NewServeMux()

	mux.HandleFunc("GET    /{$}", homeHandler) // Match only the root
	mux.HandleFunc("GET    /htmx/cats", listCats)
	mux.HandleFunc("POST   /htmx/cats", createCat)
	mux.HandleFunc("GET    /htmx/cats/{catId}", getCat)
	mux.HandleFunc("DELETE /htmx/cats/{catId}", deleteCat)

	mux.Handle("GET /content/", http.FileServerFS(content)) // Match content prefix

	return logReq(mux)
}
