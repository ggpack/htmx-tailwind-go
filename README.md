# 🔱 HTMX - Tailwind - Go templates

Sandbox project to demo 3 key technos working together:
- HTMX, lightweight client side AJAX calls relying on SSR
- Tailwind CSS a user-friendly modern way of doing CSS in 2023
- Golang templates, simple rendering of HTML hydrated with custom data

For simple webapps, the trio is intended to replace the heavyweight combo of:
- React-Queries + client-side Context / State
- React + Bootstrap / Material-UI / Ant Design
- Node + Express + JSON REST API

In the demonstrated setup, NodeJS is not a prod-dependency and only used to run the `taiwindcss CLI`.
Most of the Javascript has vanished:
- The server manages the state and it responds plain HTML to HTMX requests
- Small amount of JS code remain for event-driven behaviour


# Running

``` bash
go run .
```

# Refreshing Tailwind style file

``` bash
npx tailwindcss -i dist/main.css -o content/tailwind.css --content "**/*.html"
```
