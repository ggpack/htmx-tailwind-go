package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Cat struct {
	Name      string `json:"name"`
	BirthDate string `json:"birthDate,omitempty"`
	Color     string `json:"color,omitempty"`
	ID        string `json:"id,omitempty"`
}

type DB map[string]Cat

type CatSeletion struct {
	Db      DB
	Current string
}

// Simple in-memory database, for demo purpose
var catsDatabase = map[string]Cat{
	"Felix": {"Felix", "123", "red", ""},
	"Toto":  {"Toto", "124", "green", ""},
}

func homeHandler(res http.ResponseWriter, req *http.Request) {
	allTemplates.ExecuteTemplate(res, "index.html", "Gg")
}

func listCats(res http.ResponseWriter, req *http.Request) {
	log.Print("Listing the cats")
	allTemplates.ExecuteTemplate(res, "listing.html", CatSeletion{Db: catsDatabase})
}

func createCat(res http.ResponseWriter, req *http.Request) {
	log.Print("Creating a cat")

	// if err := req.ParseForm(); err != nil {
	// 	log.Print("Form parsing error", err)
	// }

	// for key, values := range req.PostForm {
	// 	log.Print("Form: ", key, values)
	// }

	creationDataStr := req.PostFormValue("creation-data")
	var catCreationData Cat
	err := json.Unmarshal([]byte(creationDataStr), &catCreationData)
	if err != nil {
		log.Print("Invalid JSON input")
	}

	newCatID := catCreationData.Name

	catsDatabase[newCatID] = catCreationData

	//allTemplates.ExecuteTemplate(res, "listing-item", catCreationData)
	allTemplates.ExecuteTemplate(res, "listing.html", CatSeletion{Db: catsDatabase, Current: newCatID})
}

func getCat(res http.ResponseWriter, req *http.Request) {
	catID := req.PathValue("catId")
	log.Print("Getting the cat ", catID)

	if cat, found := catsDatabase[catID]; found {
		allTemplates.ExecuteTemplate(res, "detail.html", cat)
	} else {
		res.WriteHeader(http.StatusNotFound)
	}
}

func deleteCat(res http.ResponseWriter, req *http.Request) {
	catID := req.PathValue("catId")
	log.Print("Deleting the cat ", catID)

	if _, found := catsDatabase[catID]; found {
		delete(catsDatabase, catID)
		log.Print("Deleted")
	} else {
		log.Print("Not found")
		res.WriteHeader(http.StatusNotFound)
	}
}
